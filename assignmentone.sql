-- 1. Date 
SELECT 
substring([date],1,4)+ '/' + -- Year
substring([date],7,2) + '/' + -- Month
substring([date],5,2) + ' '+-- Day
substring([date],9,2) +':' +
substring([date],11,2)
FROM sample_dataset2

-- 2. Date converted string
SELECT CONVERT(datetime, STUFF(STUFF(date,9,0,' '), 12,0,':')) 
FROM sample_dataset2

-- 3. Volume Weighted
SELECT 
SUM(vol*[close])/SUM(vol) as VOLUME_WEIGHTED_PRICE
FROM sample_dataset2
WHERE date BETWEEN '201010110900' AND '201010111300'

-- 4. COMBINING DATE, DATE CONVERSION, VMP
DECLARE @inputtime varchar(14)
SET @inputtime ='201010110900'

SELECT 
substring(@inputtime,1,4)+ '/' + -- Year
substring(@inputtime,7,2) + '/' + -- Month
substring(@inputtime,5,2) as 'Date',-- Day
substring(@inputtime,9,2) +':' +
substring(@inputtime,11,2) AS 'Start Time',
substring(@inputtime,9,2) +':' +
substring(@inputtime,11,2) AS 'End Time',
CONVERT(datetime, STUFF(STUFF(@inputtime,9,0,' '), 12,0,':')),
SUM(vol*[close])/SUM(vol) AS VOLUME_WEIGHTED_PRICE
FROM sample_dataset2

/*
CREATE PROCEDURE vwap
@indate varchar(10)
AS SELECT CONVERT(DATE, @indate, 103)
*/
-- exec vwap '20101011'