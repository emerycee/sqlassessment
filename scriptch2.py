#extract sizes of information so that mysql server can work with it

datafile=open("sample_dataset2.csv", "r")
datafile.readline()

ticker=date=vol=0
open=[0,0]
high=[0,0]
low=[0,0]
close=[0,0]

for line in datafile:
	cells=line.split(",")

	if len(cells[0]) > ticker:
		ticker=len(cells[0])
	if len(cells[1]) > date:
		date=len(cells[1])
	
	#open
	number=cells[2].split(".")
	#working out size of data that's why len is used
	if len(cells[2])>open[0]:
		open[0]=(len(cells[2]))
	try:
		if len(number[1])>open[1]:
			open[1]=(len(number[1]))
	except:
		pass

	#high
	number=cells[3].split(".")
	if len(cells[3])>high[0]:
		high[0]=(len(cells[3]))
	try:
		if len(number[1])>high[1]:
			high[1]=(len(number[1]))
	except:
		pass

	#low
	number=cells[4].split(".")
	if len(cells[4])>low[0]:
		low[0]=(len(cells[4]))
	try:
		if len(number[1])>low[1]:
			low[1]=(len(number[1]))
	except:
		pass

	#close
	number=cells[5].split(".")
	if len(cells[5])>close[0]:
		close[0]=(len(cells[5]))
	try:
		if len(number[1])>close[1]:
			close[1]=(len(number[1]))
	except:
		pass

	#volume
	if len(cells[6])>vol:
		vol=(len(cells[6]))

print ("ticker size: ", ticker)
print ("date size: ", date)
print ("open size: ", open[0], " Decimal: ", open[1])
print("high size: ", high[0], " Decimal: ", high[1])
print("low size: ", low[0], " Decimal: ", low[1])
print("close size: ", close[0], " Decimal: ", close[1])
print("vol size: ", vol)

